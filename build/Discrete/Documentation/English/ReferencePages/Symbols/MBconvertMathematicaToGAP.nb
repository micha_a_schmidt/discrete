(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     11080,        353]
NotebookOptionsPosition[      8456,        270]
NotebookOutlinePosition[     10094,        316]
CellTagsIndexPosition[     10010,        311]
WindowTitle->MBconvertMathematicaToGAP - Wolfram Mathematica
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[" ", "SymbolColorBar",
 CellMargins->{{Inherited, Inherited}, {-5, 0}}],

Cell[TextData[{
 ButtonBox["Mathematica",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Discrete/guide/Discrete"],
 StyleBox[" > ", "LinkTrailSeparator"]
}], "LinkTrail"],

Cell[BoxData[GridBox[{
   {Cell["DISCRETE PACLET SYMBOL", "PacletNameCell"], Cell[TextData[{
     Cell[BoxData[
      ActionMenuBox[
       FrameBox["\<\"Tutorials \[RightGuillemet]\"\>",
        StripOnInput->False], {"\<\"TutorialA4.nb\"\>":>
       Documentation`HelpLookup[
        If[
         StringMatchQ[
          StringReplace[{
            FrontEnd`FileName[{
              ParentDirectory[
               ParentDirectory[]], "Tutorials"}, "Tutorial A4.nb", 
             CharacterEncoding -> "UTF-8"], None}, "paclet:" :> ""], 
          "paclet*"], 
         Part[
          Part[{{"TutorialA4.nb", 
             StringReplace[{
               FrontEnd`FileName[{
                 ParentDirectory[
                  ParentDirectory[]], "Tutorials"}, "Tutorial A4.nb", 
                CharacterEncoding -> "UTF-8"], None}, "paclet:" :> ""]}}, 1], 
          2], 
         StringJoin["paclet:", 
          Part[
           Part[{{"TutorialA4.nb", 
              StringReplace[{
                FrontEnd`FileName[{
                  ParentDirectory[
                   ParentDirectory[]], "Tutorials"}, "Tutorial A4.nb", 
                 CharacterEncoding -> "UTF-8"], None}, "paclet:" :> ""]}}, 1],
            2]]]]},
       Appearance->None,
       MenuAppearance->Automatic]],
      LineSpacing->{1.4, 0}],
     "\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\
\[ThickSpace]",
     Cell[BoxData[
      ActionMenuBox[
       FrameBox["\<\"See Also \[RightGuillemet]\"\>",
        StripOnInput->False], {"\<\"MBconvertGAPToMathematica\"\>":>
       Documentation`HelpLookup[
        If[
         StringMatchQ[{"MBconvertGAPToMathematica.nb", None}, "paclet*"], 
         Part[
          
          Part[{{"MBconvertGAPToMathematica", {
             "MBconvertGAPToMathematica.nb", None}}}, 1], 2], 
         StringJoin["paclet:", 
          Part[
           
           Part[{{"MBconvertGAPToMathematica", {
              "MBconvertGAPToMathematica.nb", None}}}, 1], 2]]]]},
       Appearance->None,
       MenuAppearance->Automatic]],
      LineSpacing->{1.4, 0}],
     "\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\
\[ThickSpace]",
     Cell[BoxData[
      ActionMenuBox[
       FrameBox["\<\"URL \[RightGuillemet]\"\>",
        StripOnInput->
         False], {"\<\"Discrete/ref/MBconvertMathematicaToGAP\"\>":>
       None, "\<\"Copy Mathematica url\"\>":>
       Module[{DocumentationSearch`Private`nb$}, 
        DocumentationSearch`Private`nb$ = NotebookPut[
           Notebook[{
             Cell["Discrete/ref/MBconvertMathematicaToGAP"]}, Visible -> 
            False]]; SelectionMove[
         DocumentationSearch`Private`nb$, All, Notebook]; 
        FrontEndTokenExecute[DocumentationSearch`Private`nb$, "Copy"]; 
        NotebookClose[DocumentationSearch`Private`nb$]; Null], 
       Delimiter, "\<\"Copy web url\"\>":>
       Module[{DocumentationSearch`Private`nb$}, 
        DocumentationSearch`Private`nb$ = NotebookPut[
           Notebook[{
             Cell[
              BoxData[
               MakeBoxes[
                Hyperlink[
                "http://reference.wolfram.com/mathematica/Discrete/ref/\
MBconvertMathematicaToGAP.html"], StandardForm]], "Input", TextClipboardType -> 
              "PlainText"]}, Visible -> False]]; 
        SelectionMove[DocumentationSearch`Private`nb$, All, Notebook]; 
        FrontEndTokenExecute[DocumentationSearch`Private`nb$, "Copy"]; 
        NotebookClose[DocumentationSearch`Private`nb$]; 
        Null], "\<\"Go to web url\"\>":>FrontEndExecute[{
         NotebookLocate[{
           URL[
            StringJoin[
            "http://reference.wolfram.com/mathematica/", 
             "Discrete/ref/MBconvertMathematicaToGAP", ".html"]], None}]}]},
       Appearance->None,
       MenuAppearance->Automatic]],
      LineSpacing->{1.4, 0}]
    }], "AnchorBar"]}
  }]], "AnchorBarGrid",
 CellID->1],

Cell[CellGroupData[{

Cell["MBconvertMathematicaToGAP", "ObjectName",
 CellID->1224892054],

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{"MBconvertMathematicaToGAP", "[", "Expression", "]"}]], 
      "InlineFormula"],
     " \[LineSeparator]MBconvertMathematicaToGAP ",
     "converts Expression from Mathematica to GAP format."
    }]]}
  }]], "Usage",
 GridBoxOptions->{
 GridBoxBackground->{
  "Columns" -> {{None}}, "ColumnsIndexed" -> {}, "Rows" -> {{None}}, 
   "RowsIndexed" -> {}}},
 CellID->982511436]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[ButtonBox["EXAMPLES",
 BaseStyle->None,
 Appearance->{Automatic, None},
 Evaluator->None,
 Method->"Preemptive",
 ButtonFunction:>(FrontEndExecute[{
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], All, ButtonCell], 
    FrontEndToken["OpenCloseGroup"], 
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], After, 
     CellContents]}]& )]], "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->1319826560],

Cell[CellGroupData[{

Cell[TextData[{
 "Basic Examples",
 "\[NonBreakingSpace]\[NonBreakingSpace]",
 Cell["(1)", "ExampleCount"]
}], "ExampleSection",
 CellID->1976498622],

Cell[BoxData[
 RowBox[{
  RowBox[{"Needs", "[", "\"\<Discrete`ModelBuildingTools`\>\"", "]"}], 
  ";"}]], "Input",
 CellLabel->"In[1]:=",
 CellID->54514590],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"MBconvertMathematicaToGAP", "[", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"1", ",", 
      RowBox[{"Exp", "[", 
       RowBox[{"2", " ", "\[Pi]", " ", 
        RowBox[{"\[ImaginaryI]", "/", "3"}]}], "]"}]}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"2", "+", 
       RowBox[{"Exp", "[", 
        RowBox[{"2", "\[Pi]", " ", 
         RowBox[{"\[ImaginaryI]", "/", "5"}]}], "]"}]}], ",", 
      RowBox[{"-", "5"}]}], "}"}]}], "}"}], "]"}]], "Input",
 CellLabel->"In[2]:=",
 CellID->1611568293],

Cell[BoxData["\<\"[[1, 1*E(3)], [1*E(5) + 2, -5]]\"\>"], "Output",
 ImageSize->{261, 15},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[2]=",
 CellID->1864994181]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[ButtonBox["SEE ALSO",
 BaseStyle->None,
 Appearance->{Automatic, None},
 Evaluator->None,
 Method->"Preemptive",
 ButtonFunction:>(FrontEndExecute[{
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], All, ButtonCell], 
    FrontEndToken["OpenCloseGroup"], 
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], After, 
     CellContents]}]& )]], "SeeAlsoSection",
 CellID->1255426704],

Cell[TextData[StyleBox[ButtonBox["MBconvertGAPToMathematica",
 BaseStyle->"Hyperlink",
 ButtonData->{"MBconvertGAPToMathematica.nb", None}],
 FontFamily->"Verdana"]], "SeeAlso",
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[ButtonBox["TUTORIALS",
 BaseStyle->None,
 Appearance->{Automatic, None},
 Evaluator->None,
 Method->"Preemptive",
 ButtonFunction:>(FrontEndExecute[{
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], All, ButtonCell], 
    FrontEndToken["OpenCloseGroup"], 
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], After, 
     CellContents]}]& )]], "TutorialsSection",
 CellID->1908401893],

Cell[TextData[{
 "See ",
 ButtonBox["TutorialA4.nb",
  BaseStyle->"Hyperlink",
  ButtonData->{
    FrontEnd`FileName[{
      ParentDirectory[
       ParentDirectory[]], "Tutorials"}, "Tutorial A4.nb", CharacterEncoding -> 
     "UTF-8"], None}],
 " for an example notebook."
}], "Tutorials",
 CellID->1440733233]
}, Open  ]],

Cell[" ", "FooterCell"]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{725, 750},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"MBconvertMathematicaToGAP - Wolfram Mathematica",
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "built" -> "{2011, 11, 8, 11, 24, 7.573844}", "context" -> "Discrete`", 
    "keywords" -> {}, "index" -> True, "label" -> "Discrete Paclet Symbol", 
    "language" -> "en", "paclet" -> "Discrete", "status" -> "None", "summary" -> 
    "MBconvertMathematicaToGAP[Expression] MBconvertMathematicaToGAP converts \
Expression from Mathematica to GAP format.", "synonyms" -> {}, "title" -> 
    "MBconvertMathematicaToGAP", "type" -> "Symbol", "uri" -> 
    "Discrete/ref/MBconvertMathematicaToGAP"}, "LinkTrails" -> "", 
  "SearchTextTranslated" -> ""},
CellContext->"Global`",
FrontEndVersion->"8.0 for Linux x86 (64-bit) (February 23, 2011)",
StyleDefinitions->Notebook[{
   Cell[
    StyleData[
    StyleDefinitions -> FrontEnd`FileName[{"Wolfram"}, "Reference.nb"]]], 
   Cell[
    StyleData["Input"], CellContext -> "Global`"], 
   Cell[
    StyleData["Output"], CellContext -> "Global`"]}, Visible -> False, 
  FrontEndVersion -> "8.0 for Linux x86 (64-bit) (February 23, 2011)", 
  StyleDefinitions -> "Default.nb"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "PrimaryExamplesSection"->{
  Cell[5391, 153, 462, 13, 45, "PrimaryExamplesSection",
   CellTags->"PrimaryExamplesSection",
   CellID->1319826560]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"PrimaryExamplesSection", 9866, 304}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[618, 21, 76, 1, 3, "SymbolColorBar"],
Cell[697, 24, 182, 5, 19, "LinkTrail"],
Cell[882, 31, 3923, 97, 47, "AnchorBarGrid",
 CellID->1],
Cell[CellGroupData[{
Cell[4830, 132, 68, 1, 53, "ObjectName",
 CellID->1224892054],
Cell[4901, 135, 453, 13, 87, "Usage",
 CellID->982511436]
}, Open  ]],
Cell[CellGroupData[{
Cell[5391, 153, 462, 13, 45, "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->1319826560],
Cell[CellGroupData[{
Cell[5878, 170, 149, 5, 33, "ExampleSection",
 CellID->1976498622],
Cell[6030, 177, 156, 5, 27, "Input",
 CellID->54514590],
Cell[CellGroupData[{
Cell[6211, 186, 552, 17, 27, "Input",
 CellID->1611568293],
Cell[6766, 205, 198, 5, 36, "Output",
 CellID->1864994181]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[7025, 217, 417, 12, 45, "SeeAlsoSection",
 CellID->1255426704],
Cell[7445, 231, 197, 4, 17, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[7679, 240, 420, 12, 45, "TutorialsSection",
 CellID->1908401893],
Cell[8102, 254, 312, 11, 18, "Tutorials",
 CellID->1440733233]
}, Open  ]],
Cell[8429, 268, 23, 0, 42, "FooterCell"]
}
]
*)

(* End of internal cache information *)

