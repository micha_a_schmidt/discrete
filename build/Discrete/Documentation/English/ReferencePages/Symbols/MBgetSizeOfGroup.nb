(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     13572,        431]
NotebookOptionsPosition[     10622,        334]
NotebookOutlinePosition[     12207,        380]
CellTagsIndexPosition[     12122,        375]
WindowTitle->MBgetSizeOfGroup - Wolfram Mathematica
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[" ", "SymbolColorBar",
 CellMargins->{{Inherited, Inherited}, {-5, 0}}],

Cell[TextData[{
 StyleBox[ButtonBox["Mathematica",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:guide/Mathematica"],
  FontSlant->"Italic"],
 StyleBox[" > ", "LinkTrailSeparator"]
}], "LinkTrail"],

Cell[BoxData[GridBox[{
   {Cell["MODELBUILDING PACLET SYMBOL", "PacletNameCell"], Cell[TextData[{
     Cell[BoxData[
      ActionMenuBox[
       FrameBox["\<\"Tutorials \[RightGuillemet]\"\>",
        StripOnInput->False], {"\<\"TutorialA4.nb\"\>":>
       Documentation`HelpLookup[
        If[
         StringMatchQ[
          StringReplace[{
            FrontEnd`FileName[{
              ParentDirectory[
               ParentDirectory[]], "Tutorials"}, "Tutorial A4.nb", 
             CharacterEncoding -> "UTF-8"], None}, "paclet:" :> ""], 
          "paclet*"], 
         Part[
          Part[{{"TutorialA4.nb", 
             StringReplace[{
               FrontEnd`FileName[{
                 ParentDirectory[
                  ParentDirectory[]], "Tutorials"}, "Tutorial A4.nb", 
                CharacterEncoding -> "UTF-8"], None}, "paclet:" :> ""]}}, 1], 
          2], 
         StringJoin["paclet:", 
          Part[
           Part[{{"TutorialA4.nb", 
              StringReplace[{
                FrontEnd`FileName[{
                  ParentDirectory[
                   ParentDirectory[]], "Tutorials"}, "Tutorial A4.nb", 
                 CharacterEncoding -> "UTF-8"], None}, "paclet:" :> ""]}}, 1],
            2]]]]},
       Appearance->None,
       MenuAppearance->Automatic]],
      LineSpacing->{1.4, 0}],
     "\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\
\[ThickSpace]",
     Cell[BoxData[
      ActionMenuBox[
       FrameBox["\<\"URL \[RightGuillemet]\"\>",
        StripOnInput->False], {"\<\"ModelBuilding/ref/MBgetSizeOfGroup\"\>":>
       None, "\<\"Copy Mathematica url\"\>":>
       Module[{DocumentationSearch`Private`nb$}, 
        DocumentationSearch`Private`nb$ = NotebookPut[
           Notebook[{
             Cell["ModelBuilding/ref/MBgetSizeOfGroup"]}, Visible -> False]]; 
        SelectionMove[DocumentationSearch`Private`nb$, All, Notebook]; 
        FrontEndTokenExecute[DocumentationSearch`Private`nb$, "Copy"]; 
        NotebookClose[DocumentationSearch`Private`nb$]; Null], 
       Delimiter, "\<\"Copy web url\"\>":>
       Module[{DocumentationSearch`Private`nb$}, 
        DocumentationSearch`Private`nb$ = NotebookPut[
           Notebook[{
             Cell[
              BoxData[
               MakeBoxes[
                Hyperlink[
                "http://reference.wolfram.com/mathematica/ModelBuilding/ref/\
MBgetSizeOfGroup.html"], StandardForm]], "Input", TextClipboardType -> 
              "PlainText"]}, Visible -> False]]; 
        SelectionMove[DocumentationSearch`Private`nb$, All, Notebook]; 
        FrontEndTokenExecute[DocumentationSearch`Private`nb$, "Copy"]; 
        NotebookClose[DocumentationSearch`Private`nb$]; 
        Null], "\<\"Go to web url\"\>":>FrontEndExecute[{
         NotebookLocate[{
           URL[
            StringJoin[
            "http://reference.wolfram.com/mathematica/", 
             "ModelBuilding/ref/MBgetSizeOfGroup", ".html"]], None}]}]},
       Appearance->None,
       MenuAppearance->Automatic]],
      LineSpacing->{1.4, 0}]
    }], "AnchorBar"]}
  }]], "AnchorBarGrid",
 GridBoxOptions->{GridBoxItemSize->{"Columns" -> {
     Scaled[0.65], {
      Scaled[0.34]}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, 
   "RowsIndexed" -> {}}},
 CellID->1],

Cell[CellGroupData[{

Cell["MBgetSizeOfGroup", "ObjectName",
 CellID->1224892054],

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{"MBgetSizeOfGroup", "[", "Group", "]"}]], "InlineFormula"],
     " \[LineSeparator]MBgetSizeOfGroup returns the size of Group"
    }]]}
  }]], "Usage",
 GridBoxOptions->{
 GridBoxBackground->{
  "Columns" -> {{None}}, "ColumnsIndexed" -> {}, "Rows" -> {{None}}, 
   "RowsIndexed" -> {}}},
 CellID->982511436]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[ButtonBox["EXAMPLES",
 BaseStyle->None,
 Appearance->{Automatic, None},
 Evaluator->None,
 Method->"Preemptive",
 ButtonFunction:>(FrontEndExecute[{
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], All, ButtonCell], 
    FrontEndToken["OpenCloseGroup"], 
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], After, 
     CellContents]}]& )]], "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->730886842],

Cell[CellGroupData[{

Cell[TextData[{
 "Basic Examples",
 "\[NonBreakingSpace]\[NonBreakingSpace]",
 Cell["(1)", "ExampleCount"]
}], "ExampleSection",
 CellID->1555669210],

Cell[BoxData[
 RowBox[{
  RowBox[{"Needs", "[", "\"\<Discrete`ModelBuildingTools`\>\"", "]"}], 
  ";"}]], "Input",
 CellLabel->"In[1]:=",
 CellID->323990969],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"A4", "=", 
   RowBox[{"MBloadGAPGroup", "[", "\"\<AlternatingGroup(4)\>\"", "]"}]}], 
  ";"}]], "Input",
 CellLabel->"In[2]:=",
 CellID->2070045828],

Cell[CellGroupData[{

Cell[BoxData["\<\"starting GAP generating AlternatingGroup(4)...\"\>"], \
"Print",
 CellLabel->"During evaluation of In[9]:=",
 CellID->271479595],

Cell[BoxData["\<\"...finished\"\>"], "Print",
 CellLabel->"During evaluation of In[9]:=",
 CellID->1538483979],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"\\nStructureDescription:\"\>", 
   "\[InvisibleSpace]", "\<\"A4\"\>", "\[InvisibleSpace]", "\<\"\\n\"\>"}],
  SequenceForm["\nStructureDescription:", "A4", "\n"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[9]:=",
 CellID->2093923447],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Size of Group:\"\>", "\[InvisibleSpace]", "12", 
   "\[InvisibleSpace]", "\<\"\\n\"\>"}],
  SequenceForm["Size of Group:", 12, "\n"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[9]:=",
 CellID->480399150],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Number of irreps: \"\>", "\[InvisibleSpace]", "4", 
   "\[InvisibleSpace]", "\<\"\\n\"\>"}],
  SequenceForm["Number of irreps: ", 4, "\n"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[9]:=",
 CellID->2129400427],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Dimensions of irreps:\\n\"\>", "\[InvisibleSpace]", 
   TagBox[GridBox[{
      {"1", "2", "3", "4"},
      {"1", "1", "1", "3"}
     },
     GridBoxAlignment->{
      "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
       "RowsIndexed" -> {}},
     GridBoxSpacings->{"Columns" -> {
         Offset[0.27999999999999997`], {
          Offset[2.0999999999999996`]}, 
         Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
         Offset[0.2], {
          Offset[0.4]}, 
         Offset[0.2]}, "RowsIndexed" -> {}}],
    Function[BoxForm`e$, 
     TableForm[BoxForm`e$]]], "\[InvisibleSpace]", "\<\"\\n\"\>"}],
  SequenceForm["Dimensions of irreps:\n", 
   TableForm[{{1, 2, 3, 4}, {1, 1, 1, 3}}], "\n"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[9]:=",
 CellID->1825964087],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Character Table:\\n\"\>", "\[InvisibleSpace]", 
   TagBox[GridBox[{
      {"1", "1", "1", "1"},
      {"1", "1", 
       SuperscriptBox["\[ExponentialE]", 
        RowBox[{"-", 
         FractionBox[
          RowBox[{"2", " ", "\[ImaginaryI]", " ", "\[Pi]"}], "3"]}]], 
       SuperscriptBox["\[ExponentialE]", 
        FractionBox[
         RowBox[{"2", " ", "\[ImaginaryI]", " ", "\[Pi]"}], "3"]]},
      {"1", "1", 
       SuperscriptBox["\[ExponentialE]", 
        FractionBox[
         RowBox[{"2", " ", "\[ImaginaryI]", " ", "\[Pi]"}], "3"]], 
       SuperscriptBox["\[ExponentialE]", 
        RowBox[{"-", 
         FractionBox[
          RowBox[{"2", " ", "\[ImaginaryI]", " ", "\[Pi]"}], "3"]}]]},
      {"3", 
       RowBox[{"-", "1"}], "0", "0"}
     },
     GridBoxAlignment->{
      "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
       "RowsIndexed" -> {}},
     GridBoxSpacings->{"Columns" -> {
         Offset[0.27999999999999997`], {
          Offset[2.0999999999999996`]}, 
         Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
         Offset[0.2], {
          Offset[0.4]}, 
         Offset[0.2]}, "RowsIndexed" -> {}}],
    Function[BoxForm`e$, 
     TableForm[BoxForm`e$]]], "\[InvisibleSpace]", "\<\"\\n\"\>"}],
  SequenceForm["Character Table:\n", 
   TableForm[{{1, 1, 1, 1}, {1, 1, E^(Complex[0, 
         Rational[-2, 3]] Pi), E^(Complex[0, 
         Rational[2, 3]] Pi)}, {1, 1, E^(Complex[0, 
         Rational[2, 3]] Pi), E^(Complex[0, 
         Rational[-2, 3]] Pi)}, {3, -1, 0, 0}}], "\n"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[9]:=",
 CellID->2077658024]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"MBgetSizeOfGroup", "[", "A4", "]"}]], "Input",
 CellLabel->"In[3]:=",
 CellID->1631024333],

Cell[BoxData["12"], "Output",
 ImageSize->{20, 15},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[3]=",
 CellID->1816770794]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[ButtonBox["TUTORIALS",
 BaseStyle->None,
 Appearance->{Automatic, None},
 Evaluator->None,
 Method->"Preemptive",
 ButtonFunction:>(FrontEndExecute[{
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], All, ButtonCell], 
    FrontEndToken["OpenCloseGroup"], 
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], After, 
     CellContents]}]& )]], "TutorialsSection",
 CellID->1155968248],

Cell[TextData[{
 "See ",
 ButtonBox["TutorialA4.nb",
  BaseStyle->"Hyperlink",
  ButtonData->{
    FrontEnd`FileName[{
      ParentDirectory[
       ParentDirectory[]], "Tutorials"}, "Tutorial A4.nb", CharacterEncoding -> 
     "UTF-8"], None}],
 " for an example notebook."
}], "Tutorials",
 CellID->1295640488]
}, Open  ]],

Cell[" ", "FooterCell"]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{725, 750},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"MBgetSizeOfGroup - Wolfram Mathematica",
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "built" -> "{2011, 11, 8, 11, 25, 8.124365}", "context" -> 
    "ModelBuilding`", "keywords" -> {}, "index" -> True, "label" -> 
    "ModelBuilding Paclet Symbol", "language" -> "en", "paclet" -> 
    "ModelBuilding", "status" -> "None", "summary" -> 
    "MBgetSizeOfGroup[Group] MBgetSizeOfGroup returns the size of Group", 
    "synonyms" -> {}, "title" -> "MBgetSizeOfGroup", "type" -> "Symbol", 
    "uri" -> "ModelBuilding/ref/MBgetSizeOfGroup"}, "LinkTrails" -> "", 
  "SearchTextTranslated" -> ""},
CellContext->"Global`",
FrontEndVersion->"8.0 for Linux x86 (64-bit) (February 23, 2011)",
StyleDefinitions->Notebook[{
   Cell[
    StyleData[
    StyleDefinitions -> FrontEnd`FileName[{"Wolfram"}, "Reference.nb"]]], 
   Cell[
    StyleData["Input"], CellContext -> "Global`"], 
   Cell[
    StyleData["Output"], CellContext -> "Global`"]}, Visible -> False, 
  FrontEndVersion -> "8.0 for Linux x86 (64-bit) (February 23, 2011)", 
  StyleDefinitions -> "Default.nb"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "PrimaryExamplesSection"->{
  Cell[4687, 133, 461, 13, 45, "PrimaryExamplesSection",
   CellTags->"PrimaryExamplesSection",
   CellID->730886842]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"PrimaryExamplesSection", 11979, 368}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[609, 21, 76, 1, 3, "SymbolColorBar"],
Cell[688, 24, 209, 6, 19, "LinkTrail"],
Cell[900, 32, 3275, 78, 47, "AnchorBarGrid",
 CellID->1],
Cell[CellGroupData[{
Cell[4200, 114, 59, 1, 53, "ObjectName",
 CellID->1224892054],
Cell[4262, 117, 388, 11, 87, "Usage",
 CellID->982511436]
}, Open  ]],
Cell[CellGroupData[{
Cell[4687, 133, 461, 13, 45, "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->730886842],
Cell[CellGroupData[{
Cell[5173, 150, 149, 5, 33, "ExampleSection",
 CellID->1555669210],
Cell[5325, 157, 157, 5, 27, "Input",
 CellID->323990969],
Cell[CellGroupData[{
Cell[5507, 166, 183, 6, 27, "Input",
 CellID->2070045828],
Cell[CellGroupData[{
Cell[5715, 176, 146, 3, 23, "Print",
 CellID->271479595],
Cell[5864, 181, 110, 2, 23, "Print",
 CellID->1538483979],
Cell[5977, 185, 306, 7, 58, "Print",
 CellID->2093923447],
Cell[6286, 194, 276, 7, 41, "Print",
 CellID->480399150],
Cell[6565, 203, 283, 7, 41, "Print",
 CellID->2129400427],
Cell[6851, 212, 890, 23, 70, "Print",
 CellID->1825964087],
Cell[7744, 237, 1714, 44, 126, "Print",
 CellID->2077658024]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[9507, 287, 114, 3, 27, "Input",
 CellID->1631024333],
Cell[9624, 292, 160, 5, 36, "Output",
 CellID->1816770794]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[9845, 304, 420, 12, 45, "TutorialsSection",
 CellID->1155968248],
Cell[10268, 318, 312, 11, 18, "Tutorials",
 CellID->1295640488]
}, Open  ]],
Cell[10595, 332, 23, 0, 42, "FooterCell"]
}
]
*)

(* End of internal cache information *)

