(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     20820,        677]
NotebookOptionsPosition[     16823,        549]
NotebookOutlinePosition[     18758,        600]
CellTagsIndexPosition[     18673,        595]
WindowTitle->MBgetMajoranaMassMatrix - Wolfram Mathematica
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[" ", "SymbolColorBar",
 CellMargins->{{Inherited, Inherited}, {-5, 0}}],

Cell[TextData[{
 StyleBox[ButtonBox["Mathematica",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:guide/Mathematica"],
  FontSlant->"Italic"],
 StyleBox[" > ", "LinkTrailSeparator"]
}], "LinkTrail"],

Cell[BoxData[GridBox[{
   {Cell["MODELBUILDING PACLET SYMBOL", "PacletNameCell"], Cell[TextData[{
     Cell[BoxData[
      ActionMenuBox[
       FrameBox["\<\"Tutorials \[RightGuillemet]\"\>",
        StripOnInput->False], {"\<\"TutorialA4.nb\"\>":>
       Documentation`HelpLookup[
        If[
         StringMatchQ[
          StringReplace[{
            FrontEnd`FileName[{
              ParentDirectory[
               ParentDirectory[]], "Tutorials"}, "Tutorial A4.nb", 
             CharacterEncoding -> "UTF-8"], None}, "paclet:" :> ""], 
          "paclet*"], 
         Part[
          Part[{{"TutorialA4.nb", 
             StringReplace[{
               FrontEnd`FileName[{
                 ParentDirectory[
                  ParentDirectory[]], "Tutorials"}, "Tutorial A4.nb", 
                CharacterEncoding -> "UTF-8"], None}, "paclet:" :> ""]}}, 1], 
          2], 
         StringJoin["paclet:", 
          Part[
           Part[{{"TutorialA4.nb", 
              StringReplace[{
                FrontEnd`FileName[{
                  ParentDirectory[
                   ParentDirectory[]], "Tutorials"}, "Tutorial A4.nb", 
                 CharacterEncoding -> "UTF-8"], None}, "paclet:" :> ""]}}, 1],
            2]]]]},
       Appearance->None,
       MenuAppearance->Automatic]],
      LineSpacing->{1.4, 0}],
     "\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\
\[ThickSpace]",
     Cell[BoxData[
      ActionMenuBox[
       FrameBox["\<\"See Also \[RightGuillemet]\"\>",
        StripOnInput->False], {"\<\"MBgetMassMatrix\"\>":>
       Documentation`HelpLookup[
        If[
         StringMatchQ[{"MBgetMassMatrix.nb", None}, "paclet*"], 
         Part[
          Part[{{"MBgetMassMatrix", {"MBgetMassMatrix.nb", None}}}, 1], 2], 
         StringJoin["paclet:", 
          Part[
           Part[{{"MBgetMassMatrix", {"MBgetMassMatrix.nb", None}}}, 1], 
           2]]]]},
       Appearance->None,
       MenuAppearance->Automatic]],
      LineSpacing->{1.4, 0}],
     "\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\
\[ThickSpace]",
     Cell[BoxData[
      ActionMenuBox[
       FrameBox["\<\"URL \[RightGuillemet]\"\>",
        StripOnInput->
         False], {"\<\"ModelBuilding/ref/MBgetMajoranaMassMatrix\"\>":>
       None, "\<\"Copy Mathematica url\"\>":>
       Module[{DocumentationSearch`Private`nb$}, 
        DocumentationSearch`Private`nb$ = NotebookPut[
           Notebook[{
             Cell["ModelBuilding/ref/MBgetMajoranaMassMatrix"]}, Visible -> 
            False]]; SelectionMove[
         DocumentationSearch`Private`nb$, All, Notebook]; 
        FrontEndTokenExecute[DocumentationSearch`Private`nb$, "Copy"]; 
        NotebookClose[DocumentationSearch`Private`nb$]; Null], 
       Delimiter, "\<\"Copy web url\"\>":>
       Module[{DocumentationSearch`Private`nb$}, 
        DocumentationSearch`Private`nb$ = NotebookPut[
           Notebook[{
             Cell[
              BoxData[
               MakeBoxes[
                Hyperlink[
                "http://reference.wolfram.com/mathematica/ModelBuilding/ref/\
MBgetMajoranaMassMatrix.html"], StandardForm]], "Input", TextClipboardType -> 
              "PlainText"]}, Visible -> False]]; 
        SelectionMove[DocumentationSearch`Private`nb$, All, Notebook]; 
        FrontEndTokenExecute[DocumentationSearch`Private`nb$, "Copy"]; 
        NotebookClose[DocumentationSearch`Private`nb$]; 
        Null], "\<\"Go to web url\"\>":>FrontEndExecute[{
         NotebookLocate[{
           URL[
            StringJoin[
            "http://reference.wolfram.com/mathematica/", 
             "ModelBuilding/ref/MBgetMajoranaMassMatrix", ".html"]], None}]}]},
       Appearance->None,
       MenuAppearance->Automatic]],
      LineSpacing->{1.4, 0}]
    }], "AnchorBar"]}
  }]], "AnchorBarGrid",
 CellID->1],

Cell[CellGroupData[{

Cell["MBgetMajoranaMassMatrix", "ObjectName",
 CellID->1224892054],

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{"MBgetMajoranaMassMatrix", "[", 
       RowBox[{
       "Group", ",", "Fermion", ",", "Flavon", ",", "Order", ",", "M", ",", 
        "Y"}], "]"}]], "InlineFormula"],
     " \[LineSeparator]MBgetMajoranaMassMatrix generates the Majorana mass \
matrix ",
     Cell[BoxData[
      SubscriptBox["m", "ij"]], "InlineFormula"],
     " ",
     Cell[BoxData[
      SubscriptBox["F", "i"]], "InlineFormula"],
     " ",
     Cell[BoxData[
      SubscriptBox["F", "j"]], "InlineFormula"],
     " of with the fields Fermion ",
     Cell[BoxData[
      SubscriptBox["F", "i"]], "InlineFormula"],
     " up to order Order (default: 1) in the flavons Flavon with direct mass \
term M and Yukawa couplings starting with Y. The flavons are given as a list \
of representations, while Fermion is an ordered list of representations."
    }]]}
  }]], "Usage",
 GridBoxOptions->{
 GridBoxBackground->{
  "Columns" -> {{None}}, "ColumnsIndexed" -> {}, "Rows" -> {{None}}, 
   "RowsIndexed" -> {}}},
 CellID->982511436]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[ButtonBox["EXAMPLES",
 BaseStyle->None,
 Appearance->{Automatic, None},
 Evaluator->None,
 Method->"Preemptive",
 ButtonFunction:>(FrontEndExecute[{
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], All, ButtonCell], 
    FrontEndToken["OpenCloseGroup"], 
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], After, 
     CellContents]}]& )]], "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->1594771039],

Cell[CellGroupData[{

Cell[TextData[{
 "Basic Examples",
 "\[NonBreakingSpace]\[NonBreakingSpace]",
 Cell["(1)", "ExampleCount"]
}], "ExampleSection",
 CellID->1843434654],

Cell[BoxData[
 RowBox[{
  RowBox[{"Needs", "[", "\"\<Discrete`ModelBuildingTools`\>\"", "]"}], 
  ";"}]], "Input",
 CellLabel->"In[1]:=",
 CellID->323990969],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"A4", "=", 
   RowBox[{"MBloadGAPGroup", "[", "\"\<AlternatingGroup(4)\>\"", "]"}]}], 
  ";"}]], "Input",
 CellLabel->"In[2]:=",
 CellID->2070045828],

Cell[CellGroupData[{

Cell[BoxData["\<\"starting GAP generating AlternatingGroup(4)...\"\>"], \
"Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->1382060333],

Cell[BoxData["\<\"...finished\"\>"], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->980933626],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"\\nStructureDescription:\"\>", 
   "\[InvisibleSpace]", "\<\"A4\"\>", "\[InvisibleSpace]", "\<\"\\n\"\>"}],
  SequenceForm["\nStructureDescription:", "A4", "\n"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->677263327],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Size of Group:\"\>", "\[InvisibleSpace]", "12", 
   "\[InvisibleSpace]", "\<\"\\n\"\>"}],
  SequenceForm["Size of Group:", 12, "\n"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->451574318],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Number of irreps: \"\>", "\[InvisibleSpace]", "4", 
   "\[InvisibleSpace]", "\<\"\\n\"\>"}],
  SequenceForm["Number of irreps: ", 4, "\n"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->1733331905],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Dimensions of irreps:\\n\"\>", "\[InvisibleSpace]", 
   TagBox[GridBox[{
      {"1", "2", "3", "4"},
      {"1", "1", "1", "3"}
     },
     GridBoxAlignment->{
      "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
       "RowsIndexed" -> {}},
     GridBoxSpacings->{"Columns" -> {
         Offset[0.27999999999999997`], {
          Offset[2.0999999999999996`]}, 
         Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
         Offset[0.2], {
          Offset[0.4]}, 
         Offset[0.2]}, "RowsIndexed" -> {}}],
    Function[BoxForm`e$, 
     TableForm[BoxForm`e$]]], "\[InvisibleSpace]", "\<\"\\n\"\>"}],
  SequenceForm["Dimensions of irreps:\n", 
   TableForm[{{1, 2, 3, 4}, {1, 1, 1, 3}}], "\n"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->1103495090],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Character Table:\\n\"\>", "\[InvisibleSpace]", 
   TagBox[GridBox[{
      {"1", "1", "1", "1"},
      {"1", "1", 
       SuperscriptBox["\[ExponentialE]", 
        RowBox[{"-", 
         FractionBox[
          RowBox[{"2", " ", "\[ImaginaryI]", " ", "\[Pi]"}], "3"]}]], 
       SuperscriptBox["\[ExponentialE]", 
        FractionBox[
         RowBox[{"2", " ", "\[ImaginaryI]", " ", "\[Pi]"}], "3"]]},
      {"1", "1", 
       SuperscriptBox["\[ExponentialE]", 
        FractionBox[
         RowBox[{"2", " ", "\[ImaginaryI]", " ", "\[Pi]"}], "3"]], 
       SuperscriptBox["\[ExponentialE]", 
        RowBox[{"-", 
         FractionBox[
          RowBox[{"2", " ", "\[ImaginaryI]", " ", "\[Pi]"}], "3"]}]]},
      {"3", 
       RowBox[{"-", "1"}], "0", "0"}
     },
     GridBoxAlignment->{
      "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
       "RowsIndexed" -> {}},
     GridBoxSpacings->{"Columns" -> {
         Offset[0.27999999999999997`], {
          Offset[2.0999999999999996`]}, 
         Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
         Offset[0.2], {
          Offset[0.4]}, 
         Offset[0.2]}, "RowsIndexed" -> {}}],
    Function[BoxForm`e$, 
     TableForm[BoxForm`e$]]], "\[InvisibleSpace]", "\<\"\\n\"\>"}],
  SequenceForm["Character Table:\n", 
   TableForm[{{1, 1, 1, 1}, {1, 1, E^(Complex[0, 
         Rational[-2, 3]] Pi), E^(Complex[0, 
         Rational[2, 3]] Pi)}, {1, 1, E^(Complex[0, 
         Rational[2, 3]] Pi), E^(Complex[0, 
         Rational[-2, 3]] Pi)}, {3, -1, 0, 0}}], "\n"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->257101774]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[Chi]", "=", 
  RowBox[{"MBgetRepVector", "[", 
   RowBox[{"A4", ",", "4", ",", "\[Chi]c"}], "]"}]}]], "Input",
 CellLabel->"In[3]:=",
 CellID->245347913],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", "}"}], ",", 
   RowBox[{"{", "}"}], ",", 
   RowBox[{"{", "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"{", 
     RowBox[{"\[Chi]c1", ",", "\[Chi]c2", ",", "\[Chi]c3"}], "}"}], "}"}]}], 
  "}"}]], "Output",
 ImageSize->{243, 15},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[3]=",
 CellID->1673940458]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"$Assumptions", "=", 
  RowBox[{
   RowBox[{"Variables", "[", "\[Chi]", "]"}], "\[Element]", " ", 
   "Reals"}]}]], "Input",
 CellLabel->"In[4]:=",
 CellID->1098323210],

Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{"\[Chi]c1", "|", "\[Chi]c2", "|", "\[Chi]c3"}], ")"}], 
  "\[Element]", "Reals"}]], "Output",
 ImageSize->{183, 18},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[4]=",
 CellID->2117468095]
}, Open  ]],

Cell["\<\
The complex conjugate representations are not automatically used.\
\>", "ExampleText",
 CellID->1627589786],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"MBgetMajoranaMassMatrix", "[", 
   RowBox[{"A4", ",", 
    RowBox[{"{", "4", "}"}], ",", "\[Chi]", ",", "1", ",", "m", ",", "y"}], 
   "]"}], "//", "MatrixForm"}]], "Input",
 CellLabel->"In[5]:=",
 CellID->299407094],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {
      FractionBox[
       RowBox[{"2", " ", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"\[ImaginaryI]", " ", "imn1"}], "+", "rmn1"}], ")"}]}], 
       SqrtBox["3"]], 
      FractionBox[
       RowBox[{
        RowBox[{"(", 
         RowBox[{
          RowBox[{"\[ImaginaryI]", " ", "iy1n1"}], "+", "ry1n1"}], ")"}], " ",
         "\[Chi]c3"}], 
       SqrtBox["3"]], 
      FractionBox[
       RowBox[{
        RowBox[{"(", 
         RowBox[{
          RowBox[{"\[ImaginaryI]", " ", "iy1n1"}], "+", "ry1n1"}], ")"}], " ",
         "\[Chi]c2"}], 
       SqrtBox["3"]]},
     {
      FractionBox[
       RowBox[{
        RowBox[{"(", 
         RowBox[{
          RowBox[{"\[ImaginaryI]", " ", "iy1n1"}], "+", "ry1n1"}], ")"}], " ",
         "\[Chi]c3"}], 
       SqrtBox["3"]], 
      FractionBox[
       RowBox[{"2", " ", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"\[ImaginaryI]", " ", "imn1"}], "+", "rmn1"}], ")"}]}], 
       SqrtBox["3"]], 
      FractionBox[
       RowBox[{
        RowBox[{"(", 
         RowBox[{
          RowBox[{"\[ImaginaryI]", " ", "iy1n1"}], "+", "ry1n1"}], ")"}], " ",
         "\[Chi]c1"}], 
       SqrtBox["3"]]},
     {
      FractionBox[
       RowBox[{
        RowBox[{"(", 
         RowBox[{
          RowBox[{"\[ImaginaryI]", " ", "iy1n1"}], "+", "ry1n1"}], ")"}], " ",
         "\[Chi]c2"}], 
       SqrtBox["3"]], 
      FractionBox[
       RowBox[{
        RowBox[{"(", 
         RowBox[{
          RowBox[{"\[ImaginaryI]", " ", "iy1n1"}], "+", "ry1n1"}], ")"}], " ",
         "\[Chi]c1"}], 
       SqrtBox["3"]], 
      FractionBox[
       RowBox[{"2", " ", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"\[ImaginaryI]", " ", "imn1"}], "+", "rmn1"}], ")"}]}], 
       SqrtBox["3"]]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 ImageSize->{399, 81},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[5]//MatrixForm=",
 CellID->2027969813]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData["$Assumptions"], "Input",
 CellLabel->"In[6]:=",
 CellID->1145742942],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"(", 
    RowBox[{"\[Chi]c1", "|", "\[Chi]c2", "|", "\[Chi]c3"}], ")"}], 
   "\[Element]", "Reals"}], "&&", 
  RowBox[{"rmn1", "\[Element]", "Reals"}], "&&", 
  RowBox[{"imn1", "\[Element]", "Reals"}], "&&", 
  RowBox[{"ry1n1", "\[Element]", "Reals"}], "&&", 
  RowBox[{"iy1n1", "\[Element]", "Reals"}]}]], "Output",
 ImageSize->{342, 35},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[6]=",
 CellID->661389227]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[ButtonBox["SEE ALSO",
 BaseStyle->None,
 Appearance->{Automatic, None},
 Evaluator->None,
 Method->"Preemptive",
 ButtonFunction:>(FrontEndExecute[{
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], All, ButtonCell], 
    FrontEndToken["OpenCloseGroup"], 
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], After, 
     CellContents]}]& )]], "SeeAlsoSection",
 CellID->1255426704],

Cell[TextData[StyleBox[ButtonBox["MBgetMassMatrix",
 BaseStyle->"Hyperlink",
 ButtonData->{"MBgetMassMatrix.nb", None}],
 FontFamily->"Verdana"]], "SeeAlso",
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[ButtonBox["TUTORIALS",
 BaseStyle->None,
 Appearance->{Automatic, None},
 Evaluator->None,
 Method->"Preemptive",
 ButtonFunction:>(FrontEndExecute[{
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], All, ButtonCell], 
    FrontEndToken["OpenCloseGroup"], 
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], After, 
     CellContents]}]& )]], "TutorialsSection",
 CellID->1155968248],

Cell[TextData[{
 "See ",
 ButtonBox["TutorialA4.nb",
  BaseStyle->"Hyperlink",
  ButtonData->{
    FrontEnd`FileName[{
      ParentDirectory[
       ParentDirectory[]], "Tutorials"}, "Tutorial A4.nb", CharacterEncoding -> 
     "UTF-8"], None}],
 " for an example notebook."
}], "Tutorials",
 CellID->1295640488]
}, Open  ]],

Cell[" ", "FooterCell"]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{725, 750},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"MBgetMajoranaMassMatrix - Wolfram Mathematica",
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "built" -> "{2011, 11, 8, 11, 24, 48.428836}", "context" -> 
    "ModelBuilding`", "keywords" -> {}, "index" -> True, "label" -> 
    "ModelBuilding Paclet Symbol", "language" -> "en", "paclet" -> 
    "ModelBuilding", "status" -> "None", "summary" -> 
    "MBgetMajoranaMassMatrix[Group, Fermion, Flavon, Order, M, Y] \
MBgetMajoranaMassMatrix generates the Majorana mass matrix m_ij F_i F_j of \
with the fields Fermion F_i up to order Order (default: 1) in the flavons \
Flavon with direct mass term M and Yukawa couplings starting with Y. The \
flavons are given as a list of representations, while Fermion is an ordered \
list of representations.", "synonyms" -> {}, "title" -> 
    "MBgetMajoranaMassMatrix", "type" -> "Symbol", "uri" -> 
    "ModelBuilding/ref/MBgetMajoranaMassMatrix"}, "LinkTrails" -> "", 
  "SearchTextTranslated" -> ""},
CellContext->"Global`",
FrontEndVersion->"8.0 for Linux x86 (64-bit) (February 23, 2011)",
StyleDefinitions->Notebook[{
   Cell[
    StyleData[
    StyleDefinitions -> FrontEnd`FileName[{"Wolfram"}, "Reference.nb"]]], 
   Cell[
    StyleData["Input"], CellContext -> "Global`"], 
   Cell[
    StyleData["Output"], CellContext -> "Global`"]}, Visible -> False, 
  FrontEndVersion -> "8.0 for Linux x86 (64-bit) (February 23, 2011)", 
  StyleDefinitions -> "Default.nb"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "PrimaryExamplesSection"->{
  Cell[5955, 167, 462, 13, 45, "PrimaryExamplesSection",
   CellTags->"PrimaryExamplesSection",
   CellID->1594771039]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"PrimaryExamplesSection", 18529, 588}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[616, 21, 76, 1, 3, "SymbolColorBar"],
Cell[695, 24, 209, 6, 19, "LinkTrail"],
Cell[907, 32, 3840, 94, 47, "AnchorBarGrid",
 CellID->1],
Cell[CellGroupData[{
Cell[4772, 130, 66, 1, 53, "ObjectName",
 CellID->1224892054],
Cell[4841, 133, 1077, 29, 138, "Usage",
 CellID->982511436]
}, Open  ]],
Cell[CellGroupData[{
Cell[5955, 167, 462, 13, 45, "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->1594771039],
Cell[CellGroupData[{
Cell[6442, 184, 149, 5, 33, "ExampleSection",
 CellID->1843434654],
Cell[6594, 191, 157, 5, 27, "Input",
 CellID->323990969],
Cell[CellGroupData[{
Cell[6776, 200, 183, 6, 27, "Input",
 CellID->2070045828],
Cell[CellGroupData[{
Cell[6984, 210, 147, 3, 23, "Print",
 CellID->1382060333],
Cell[7134, 215, 109, 2, 23, "Print",
 CellID->980933626],
Cell[7246, 219, 305, 7, 58, "Print",
 CellID->677263327],
Cell[7554, 228, 276, 7, 41, "Print",
 CellID->451574318],
Cell[7833, 237, 283, 7, 41, "Print",
 CellID->1733331905],
Cell[8119, 246, 890, 23, 70, "Print",
 CellID->1103495090],
Cell[9012, 271, 1713, 44, 126, "Print",
 CellID->257101774]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[10774, 321, 179, 5, 27, "Input",
 CellID->245347913],
Cell[10956, 328, 391, 14, 36, "Output",
 CellID->1673940458]
}, Open  ]],
Cell[CellGroupData[{
Cell[11384, 347, 191, 6, 27, "Input",
 CellID->1098323210],
Cell[11578, 355, 276, 9, 39, "Output",
 CellID->2117468095]
}, Open  ]],
Cell[11869, 367, 117, 3, 33, "ExampleText",
 CellID->1627589786],
Cell[CellGroupData[{
Cell[12011, 374, 251, 7, 27, "Input",
 CellID->299407094],
Cell[12265, 383, 2471, 83, 115, "Output",
 CellID->2027969813]
}, Open  ]],
Cell[CellGroupData[{
Cell[14773, 471, 82, 2, 27, "Input",
 CellID->1145742942],
Cell[14858, 475, 493, 14, 56, "Output",
 CellID->661389227]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[15412, 496, 417, 12, 45, "SeeAlsoSection",
 CellID->1255426704],
Cell[15832, 510, 177, 4, 17, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[16046, 519, 420, 12, 45, "TutorialsSection",
 CellID->1155968248],
Cell[16469, 533, 312, 11, 18, "Tutorials",
 CellID->1295640488]
}, Open  ]],
Cell[16796, 547, 23, 0, 42, "FooterCell"]
}
]
*)

(* End of internal cache information *)

