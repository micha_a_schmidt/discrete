(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     19939,        586]
NotebookOptionsPosition[     16718,        483]
NotebookOutlinePosition[     18426,        531]
CellTagsIndexPosition[     18341,        526]
WindowTitle->MBloadGAPGroup - Wolfram Mathematica
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[" ", "SymbolColorBar",
 CellMargins->{{Inherited, Inherited}, {-5, 0}}],

Cell[TextData[{
 StyleBox[ButtonBox["Mathematica",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:guide/Mathematica"],
  FontSlant->"Italic"],
 StyleBox[" > ", "LinkTrailSeparator"]
}], "LinkTrail"],

Cell[BoxData[GridBox[{
   {Cell["MODELBUILDING PACLET SYMBOL", "PacletNameCell"], Cell[TextData[{
     Cell[BoxData[
      ActionMenuBox[
       FrameBox["\<\"Tutorials \[RightGuillemet]\"\>",
        StripOnInput->False], {
       RowBox[{
        RowBox[{"{", "}"}], "\[LeftDoubleBracket]", "1", 
        "\[RightDoubleBracket]"}]:>Documentation`HelpLookup[
        If[
         StringMatchQ[
          Part[{}, 2], "paclet*"], 
         Part[
          Part[{{}, {}, {}, {"TutorialA4.nb", 
             StringReplace[{
               FrontEnd`FileName[{
                 ParentDirectory[
                  ParentDirectory[]], "Tutorials"}, "Tutorial A4.nb", 
                CharacterEncoding -> "UTF-8"], None}, "paclet:" :> ""]}}, 1], 
          2], 
         StringJoin["paclet:", 
          Part[
           Part[{{}, {}, {}, {"TutorialA4.nb", 
              StringReplace[{
                FrontEnd`FileName[{
                  ParentDirectory[
                   ParentDirectory[]], "Tutorials"}, "Tutorial A4.nb", 
                 CharacterEncoding -> "UTF-8"], None}, "paclet:" :> ""]}}, 1],
            2]]]], 
       RowBox[{
        RowBox[{"{", "}"}], "\[LeftDoubleBracket]", "1", 
        "\[RightDoubleBracket]"}]:>Documentation`HelpLookup[
        If[
         StringMatchQ[
          Part[{}, 2], "paclet*"], 
         Part[
          Part[{{}, {}, {}, {"TutorialA4.nb", 
             StringReplace[{
               FrontEnd`FileName[{
                 ParentDirectory[
                  ParentDirectory[]], "Tutorials"}, "Tutorial A4.nb", 
                CharacterEncoding -> "UTF-8"], None}, "paclet:" :> ""]}}, 2], 
          2], 
         StringJoin["paclet:", 
          Part[
           Part[{{}, {}, {}, {"TutorialA4.nb", 
              StringReplace[{
                FrontEnd`FileName[{
                  ParentDirectory[
                   ParentDirectory[]], "Tutorials"}, "Tutorial A4.nb", 
                 CharacterEncoding -> "UTF-8"], None}, "paclet:" :> ""]}}, 2],
            2]]]], 
       RowBox[{
        RowBox[{"{", "}"}], "\[LeftDoubleBracket]", "1", 
        "\[RightDoubleBracket]"}]:>Documentation`HelpLookup[
        If[
         StringMatchQ[
          Part[{}, 2], "paclet*"], 
         Part[
          Part[{{}, {}, {}, {"TutorialA4.nb", 
             StringReplace[{
               FrontEnd`FileName[{
                 ParentDirectory[
                  ParentDirectory[]], "Tutorials"}, "Tutorial A4.nb", 
                CharacterEncoding -> "UTF-8"], None}, "paclet:" :> ""]}}, 3], 
          2], 
         StringJoin["paclet:", 
          Part[
           Part[{{}, {}, {}, {"TutorialA4.nb", 
              StringReplace[{
                FrontEnd`FileName[{
                  ParentDirectory[
                   ParentDirectory[]], "Tutorials"}, "Tutorial A4.nb", 
                 CharacterEncoding -> "UTF-8"], None}, "paclet:" :> ""]}}, 3],
            2]]]], "\<\"TutorialA4.nb\"\>":>Documentation`HelpLookup[
        If[
         StringMatchQ[
          StringReplace[{
            FrontEnd`FileName[{
              ParentDirectory[
               ParentDirectory[]], "Tutorials"}, "Tutorial A4.nb", 
             CharacterEncoding -> "UTF-8"], None}, "paclet:" :> ""], 
          "paclet*"], 
         Part[
          Part[{{}, {}, {}, {"TutorialA4.nb", 
             StringReplace[{
               FrontEnd`FileName[{
                 ParentDirectory[
                  ParentDirectory[]], "Tutorials"}, "Tutorial A4.nb", 
                CharacterEncoding -> "UTF-8"], None}, "paclet:" :> ""]}}, 4], 
          2], 
         StringJoin["paclet:", 
          Part[
           Part[{{}, {}, {}, {"TutorialA4.nb", 
              StringReplace[{
                FrontEnd`FileName[{
                  ParentDirectory[
                   ParentDirectory[]], "Tutorials"}, "Tutorial A4.nb", 
                 CharacterEncoding -> "UTF-8"], None}, "paclet:" :> ""]}}, 4],
            2]]]]},
       Appearance->None,
       MenuAppearance->Automatic]],
      LineSpacing->{1.4, 0}],
     "\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\
\[ThickSpace]",
     Cell[BoxData[
      ActionMenuBox[
       FrameBox["\<\"See Also \[RightGuillemet]\"\>",
        StripOnInput->False], {"\<\"MBloadGAPSmallGroup\"\>":>
       Documentation`HelpLookup[
        If[
         StringMatchQ[{"MBloadGAPSmallGroup.nb", None}, "paclet*"], 
         Part[
          
          Part[{{"MBloadGAPSmallGroup", {"MBloadGAPSmallGroup.nb", None}}, {
            "MBloadGroupManually, ", {"MBloadGroupManually.nb", None}}, {
            "MBloadGeneratedGAPGroup", {"MBloadGeneratedGAPGroup.nb", None}}},
            1], 2], 
         StringJoin["paclet:", 
          Part[
           
           Part[{{"MBloadGAPSmallGroup", {"MBloadGAPSmallGroup.nb", None}}, {
             "MBloadGroupManually, ", {"MBloadGroupManually.nb", None}}, {
             "MBloadGeneratedGAPGroup", {
              "MBloadGeneratedGAPGroup.nb", None}}}, 1], 
           2]]]], "\<\"MBloadGroupManually, \"\>":>Documentation`HelpLookup[
        If[
         StringMatchQ[{"MBloadGroupManually.nb", None}, "paclet*"], 
         Part[
          
          Part[{{"MBloadGAPSmallGroup", {"MBloadGAPSmallGroup.nb", None}}, {
            "MBloadGroupManually, ", {"MBloadGroupManually.nb", None}}, {
            "MBloadGeneratedGAPGroup", {"MBloadGeneratedGAPGroup.nb", None}}},
            2], 2], 
         StringJoin["paclet:", 
          Part[
           
           Part[{{"MBloadGAPSmallGroup", {"MBloadGAPSmallGroup.nb", None}}, {
             "MBloadGroupManually, ", {"MBloadGroupManually.nb", None}}, {
             "MBloadGeneratedGAPGroup", {
              "MBloadGeneratedGAPGroup.nb", None}}}, 2], 
           2]]]], "\<\"MBloadGeneratedGAPGroup\"\>":>Documentation`HelpLookup[
       
        If[
         StringMatchQ[{"MBloadGeneratedGAPGroup.nb", None}, "paclet*"], 
         Part[
          
          Part[{{"MBloadGAPSmallGroup", {"MBloadGAPSmallGroup.nb", None}}, {
            "MBloadGroupManually, ", {"MBloadGroupManually.nb", None}}, {
            "MBloadGeneratedGAPGroup", {"MBloadGeneratedGAPGroup.nb", None}}},
            3], 2], 
         StringJoin["paclet:", 
          Part[
           
           Part[{{"MBloadGAPSmallGroup", {"MBloadGAPSmallGroup.nb", None}}, {
             "MBloadGroupManually, ", {"MBloadGroupManually.nb", None}}, {
             "MBloadGeneratedGAPGroup", {
              "MBloadGeneratedGAPGroup.nb", None}}}, 3], 2]]]]},
       Appearance->None,
       MenuAppearance->Automatic]],
      LineSpacing->{1.4, 0}],
     "\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\
\[ThickSpace]",
     Cell[BoxData[
      ActionMenuBox[
       FrameBox["\<\"URL \[RightGuillemet]\"\>",
        StripOnInput->False], {"\<\"ModelBuilding/ref/MBloadGAPGroup\"\>":>
       None, "\<\"Copy Mathematica url\"\>":>
       Module[{DocumentationSearch`Private`nb$}, 
        DocumentationSearch`Private`nb$ = NotebookPut[
           Notebook[{
             Cell["ModelBuilding/ref/MBloadGAPGroup"]}, Visible -> False]]; 
        SelectionMove[DocumentationSearch`Private`nb$, All, Notebook]; 
        FrontEndTokenExecute[DocumentationSearch`Private`nb$, "Copy"]; 
        NotebookClose[DocumentationSearch`Private`nb$]; Null], 
       Delimiter, "\<\"Copy web url\"\>":>
       Module[{DocumentationSearch`Private`nb$}, 
        DocumentationSearch`Private`nb$ = NotebookPut[
           Notebook[{
             Cell[
              BoxData[
               MakeBoxes[
                Hyperlink[
                "http://reference.wolfram.com/mathematica/ModelBuilding/ref/\
MBloadGAPGroup.html"], StandardForm]], "Input", TextClipboardType -> 
              "PlainText"]}, Visible -> False]]; 
        SelectionMove[DocumentationSearch`Private`nb$, All, Notebook]; 
        FrontEndTokenExecute[DocumentationSearch`Private`nb$, "Copy"]; 
        NotebookClose[DocumentationSearch`Private`nb$]; 
        Null], "\<\"Go to web url\"\>":>FrontEndExecute[{
         NotebookLocate[{
           URL[
            StringJoin[
            "http://reference.wolfram.com/mathematica/", 
             "ModelBuilding/ref/MBloadGAPGroup", ".html"]], None}]}]},
       Appearance->None,
       MenuAppearance->Automatic]],
      LineSpacing->{1.4, 0}]
    }], "AnchorBar"]}
  }]], "AnchorBarGrid",
 CellID->1],

Cell[CellGroupData[{

Cell["MBloadGAPGroup", "ObjectName",
 CellID->1224892054],

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{"MBloadGAPGroup", "[", 
       RowBox[{"Name", ",", "Options"}], "]"}]], "InlineFormula"],
     " \[LineSeparator]MBloadGAPGroup loads the group given by the string \
Name, which should refer to a GAP command, e.g. \"SmallGroup(12,3)\" or \
AlternatingGroup(4) would load ",
     Cell[BoxData[
      SubscriptBox["A", "4"]], "InlineFormula"],
     "."
    }]]}
  }]], "Usage",
 GridBoxOptions->{
 GridBoxBackground->{
  "Columns" -> {{None}}, "ColumnsIndexed" -> {}, "Rows" -> {{None}}, 
   "RowsIndexed" -> {}}},
 CellID->982511436]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[ButtonBox["EXAMPLES",
 BaseStyle->None,
 Appearance->{Automatic, None},
 Evaluator->None,
 Method->"Preemptive",
 ButtonFunction:>(FrontEndExecute[{
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], All, ButtonCell], 
    FrontEndToken["OpenCloseGroup"], 
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], After, 
     CellContents]}]& )]], "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->1718351944],

Cell[CellGroupData[{

Cell[TextData[{
 "Basic Examples",
 "\[NonBreakingSpace]\[NonBreakingSpace]",
 Cell["(1)", "ExampleCount"]
}], "ExampleSection",
 CellID->1401587804],

Cell[BoxData[
 RowBox[{
  RowBox[{"Needs", "[", "\"\<Discrete`ModelBuildingTools`\>\"", "]"}], 
  ";"}]], "Input",
 CellLabel->"In[1]:=",
 CellID->323990969],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"Group", "=", 
   RowBox[{"MBloadGAPGroup", "[", "\"\<AlternatingGroup(4)\>\"", "]"}]}], 
  ";"}]], "Input",
 CellLabel->"In[2]:=",
 CellID->805278218],

Cell[CellGroupData[{

Cell[BoxData["\<\"starting GAP generating AlternatingGroup(4)...\"\>"], \
"Print",
 CellLabel->"During evaluation of In[1]:=",
 CellID->1228997481],

Cell[BoxData["\<\"...finished\"\>"], "Print",
 CellLabel->"During evaluation of In[1]:=",
 CellID->184934688],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"\\nStructureDescription:\"\>", 
   "\[InvisibleSpace]", "\<\"A4\"\>", "\[InvisibleSpace]", "\<\"\\n\"\>"}],
  SequenceForm["\nStructureDescription:", "A4", "\n"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[1]:=",
 CellID->245708593],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Size of Group:\"\>", "\[InvisibleSpace]", "12", 
   "\[InvisibleSpace]", "\<\"\\n\"\>"}],
  SequenceForm["Size of Group:", 12, "\n"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[1]:=",
 CellID->246814306],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Number of irreps: \"\>", "\[InvisibleSpace]", "4", 
   "\[InvisibleSpace]", "\<\"\\n\"\>"}],
  SequenceForm["Number of irreps: ", 4, "\n"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[1]:=",
 CellID->1683835713],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Dimensions of irreps:\\n\"\>", "\[InvisibleSpace]", 
   TagBox[GridBox[{
      {"1", "2", "3", "4"},
      {"1", "1", "1", "3"}
     },
     GridBoxAlignment->{
      "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
       "RowsIndexed" -> {}},
     GridBoxSpacings->{"Columns" -> {
         Offset[0.27999999999999997`], {
          Offset[2.0999999999999996`]}, 
         Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
         Offset[0.2], {
          Offset[0.4]}, 
         Offset[0.2]}, "RowsIndexed" -> {}}],
    Function[BoxForm`e$, 
     TableForm[BoxForm`e$]]], "\[InvisibleSpace]", "\<\"\\n\"\>"}],
  SequenceForm["Dimensions of irreps:\n", 
   TableForm[{{1, 2, 3, 4}, {1, 1, 1, 3}}], "\n"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[1]:=",
 CellID->300472514],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Character Table:\\n\"\>", "\[InvisibleSpace]", 
   TagBox[GridBox[{
      {"1", "1", "1", "1"},
      {"1", "1", 
       SuperscriptBox["\[ExponentialE]", 
        RowBox[{"-", 
         FractionBox[
          RowBox[{"2", " ", "\[ImaginaryI]", " ", "\[Pi]"}], "3"]}]], 
       SuperscriptBox["\[ExponentialE]", 
        FractionBox[
         RowBox[{"2", " ", "\[ImaginaryI]", " ", "\[Pi]"}], "3"]]},
      {"1", "1", 
       SuperscriptBox["\[ExponentialE]", 
        FractionBox[
         RowBox[{"2", " ", "\[ImaginaryI]", " ", "\[Pi]"}], "3"]], 
       SuperscriptBox["\[ExponentialE]", 
        RowBox[{"-", 
         FractionBox[
          RowBox[{"2", " ", "\[ImaginaryI]", " ", "\[Pi]"}], "3"]}]]},
      {"3", 
       RowBox[{"-", "1"}], "0", "0"}
     },
     GridBoxAlignment->{
      "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
       "RowsIndexed" -> {}},
     GridBoxSpacings->{"Columns" -> {
         Offset[0.27999999999999997`], {
          Offset[2.0999999999999996`]}, 
         Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
         Offset[0.2], {
          Offset[0.4]}, 
         Offset[0.2]}, "RowsIndexed" -> {}}],
    Function[BoxForm`e$, 
     TableForm[BoxForm`e$]]], "\[InvisibleSpace]", "\<\"\\n\"\>"}],
  SequenceForm["Character Table:\n", 
   TableForm[{{1, 1, 1, 1}, {1, 1, E^(Complex[0, 
         Rational[-2, 3]] Pi), E^(Complex[0, 
         Rational[2, 3]] Pi)}, {1, 1, E^(Complex[0, 
         Rational[2, 3]] Pi), E^(Complex[0, 
         Rational[-2, 3]] Pi)}, {3, -1, 0, 0}}], "\n"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[1]:=",
 CellID->1768277479]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[ButtonBox["SEE ALSO",
 BaseStyle->None,
 Appearance->{Automatic, None},
 Evaluator->None,
 Method->"Preemptive",
 ButtonFunction:>(FrontEndExecute[{
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], All, ButtonCell], 
    FrontEndToken["OpenCloseGroup"], 
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], After, 
     CellContents]}]& )]], "SeeAlsoSection",
 CellID->710151633],

Cell[TextData[{
 StyleBox[ButtonBox["MBloadGAPSmallGroup",
  BaseStyle->"Hyperlink",
  ButtonData->{"MBloadGAPSmallGroup.nb", None}],
  FontFamily->"Verdana"],
 ", ",
 StyleBox[ButtonBox["MBloadGroupManually, ",
  BaseStyle->"Hyperlink",
  ButtonData->{"MBloadGroupManually.nb", None}],
  FontFamily->"Verdana"],
 StyleBox[ButtonBox["MBloadGeneratedGAPGroup",
  BaseStyle->"Hyperlink",
  ButtonData->{"MBloadGeneratedGAPGroup.nb", None}],
  FontFamily->"Verdana"]
}], "SeeAlso",
 CellID->1401793305]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[ButtonBox["TUTORIALS",
 BaseStyle->None,
 Appearance->{Automatic, None},
 Evaluator->None,
 Method->"Preemptive",
 ButtonFunction:>(FrontEndExecute[{
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], All, ButtonCell], 
    FrontEndToken["OpenCloseGroup"], 
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], After, 
     CellContents]}]& )]], "TutorialsSection",
 CellID->514489869],

Cell["This function allows for two options:", "Tutorials",
 CellID->1784740454],

Cell["\<\
\"CheckUnitarity\" controls whether there is a check for unitarity of all \
representations. The default is False, as this check is expensive.\
\>", "Tutorials",
 CellID->222458952],

Cell["\<\
\"NumericalEvaluate\" forces the package to evaluate the Clebsch-Gordan \
coefficients numerically. The default is False.\
\>", "Tutorials",
 CellID->701400908]
}, Open  ]],

Cell[" ", "FooterCell"]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{725, 750},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"MBloadGAPGroup - Wolfram Mathematica",
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "built" -> "{2011, 11, 8, 11, 25, 26.247443}", "context" -> 
    "ModelBuilding`", "keywords" -> {}, "index" -> True, "label" -> 
    "ModelBuilding Paclet Symbol", "language" -> "en", "paclet" -> 
    "ModelBuilding", "status" -> "None", "summary" -> 
    "MBloadGAPGroup[Name, Options] MBloadGAPGroup loads the group given by \
the string Name, which should refer to a GAP command, e.g. \"SmallGroup(12,3)\
\" or AlternatingGroup(4) would load A_4.", "synonyms" -> {}, "title" -> 
    "MBloadGAPGroup", "type" -> "Symbol", "uri" -> 
    "ModelBuilding/ref/MBloadGAPGroup"}, "LinkTrails" -> "", 
  "SearchTextTranslated" -> ""},
CellContext->"Global`",
FrontEndVersion->"8.0 for Linux x86 (64-bit) (February 23, 2011)",
StyleDefinitions->Notebook[{
   Cell[
    StyleData[
    StyleDefinitions -> FrontEnd`FileName[{"Wolfram"}, "Reference.nb"]]], 
   Cell[
    StyleData["Input"], CellContext -> "Global`"], 
   Cell[
    StyleData["Output"], CellContext -> "Global`"]}, Visible -> False, 
  FrontEndVersion -> "8.0 for Linux x86 (64-bit) (February 23, 2011)", 
  StyleDefinitions -> "Default.nb"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "PrimaryExamplesSection"->{
  Cell[10008, 261, 462, 13, 45, "PrimaryExamplesSection",
   CellTags->"PrimaryExamplesSection",
   CellID->1718351944]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"PrimaryExamplesSection", 18196, 519}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[607, 21, 76, 1, 3, "SymbolColorBar"],
Cell[686, 24, 209, 6, 19, "LinkTrail"],
Cell[898, 32, 8375, 200, 47, "AnchorBarGrid",
 CellID->1],
Cell[CellGroupData[{
Cell[9298, 236, 57, 1, 53, "ObjectName",
 CellID->1224892054],
Cell[9358, 239, 613, 17, 104, "Usage",
 CellID->982511436]
}, Open  ]],
Cell[CellGroupData[{
Cell[10008, 261, 462, 13, 45, "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->1718351944],
Cell[CellGroupData[{
Cell[10495, 278, 149, 5, 33, "ExampleSection",
 CellID->1401587804],
Cell[10647, 285, 157, 5, 27, "Input",
 CellID->323990969],
Cell[CellGroupData[{
Cell[10829, 294, 185, 6, 27, "Input",
 CellID->805278218],
Cell[CellGroupData[{
Cell[11039, 304, 147, 3, 23, "Print",
 CellID->1228997481],
Cell[11189, 309, 109, 2, 23, "Print",
 CellID->184934688],
Cell[11301, 313, 305, 7, 58, "Print",
 CellID->245708593],
Cell[11609, 322, 276, 7, 41, "Print",
 CellID->246814306],
Cell[11888, 331, 283, 7, 41, "Print",
 CellID->1683835713],
Cell[12174, 340, 889, 23, 70, "Print",
 CellID->300472514],
Cell[13066, 365, 1714, 44, 126, "Print",
 CellID->1768277479]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[14853, 417, 416, 12, 70, "SeeAlsoSection",
 CellID->710151633],
Cell[15272, 431, 499, 15, 70, "SeeAlso",
 CellID->1401793305]
}, Open  ]],
Cell[CellGroupData[{
Cell[15808, 451, 419, 12, 70, "TutorialsSection",
 CellID->514489869],
Cell[16230, 465, 79, 1, 70, "Tutorials",
 CellID->1784740454],
Cell[16312, 468, 191, 4, 70, "Tutorials",
 CellID->222458952],
Cell[16506, 474, 170, 4, 70, "Tutorials",
 CellID->701400908]
}, Open  ]],
Cell[16691, 481, 23, 0, 70, "FooterCell"]
}
]
*)

(* End of internal cache information *)

